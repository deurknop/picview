# Picture viewer

A simple BMP picture viewer.
It only supports 24-bit encoded BMP files, maximum size 161x81 pixels.
Nothing fancy.

## Getting started

Install the app, and place pictures in a pics/ folder on your device.
